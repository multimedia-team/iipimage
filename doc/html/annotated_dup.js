var annotated_dup =
[
    [ "Cache", "classCache.html", "classCache" ],
    [ "CMP", "classCMP.html", "classCMP" ],
    [ "CNT", "classCNT.html", "classCNT" ],
    [ "COL", "classCOL.html", "classCOL" ],
    [ "Compressor", "classCompressor.html", "classCompressor" ],
    [ "CTW", "classCTW.html", "classCTW" ],
    [ "CVT", "classCVT.html", "classCVT" ],
    [ "DeepZoom", "classDeepZoom.html", "classDeepZoom" ],
    [ "Environment", "classEnvironment.html", null ],
    [ "FCGIWriter", "classFCGIWriter.html", "classFCGIWriter" ],
    [ "FIF", "classFIF.html", "classFIF" ],
    [ "file_error", "classfile__error.html", "classfile__error" ],
    [ "FileWriter", "classFileWriter.html", "classFileWriter" ],
    [ "GAM", "classGAM.html", "classGAM" ],
    [ "HEI", "classHEI.html", "classHEI" ],
    [ "ICC", "classICC.html", "classICC" ],
    [ "IIIF", "classIIIF.html", "classIIIF" ],
    [ "iip_destination_mgr", "structiip__destination__mgr.html", "structiip__destination__mgr" ],
    [ "IIPImage", "classIIPImage.html", "classIIPImage" ],
    [ "IIPResponse", "classIIPResponse.html", "classIIPResponse" ],
    [ "INV", "classINV.html", "classINV" ],
    [ "JPEGCompressor", "classJPEGCompressor.html", "classJPEGCompressor" ],
    [ "JTL", "classJTL.html", "classJTL" ],
    [ "JTLS", "classJTLS.html", "classJTLS" ],
    [ "KakaduImage", "classKakaduImage.html", "classKakaduImage" ],
    [ "kdu_stream_message", "classkdu__stream__message.html", "classkdu__stream__message" ],
    [ "Logger", "classLogger.html", "classLogger" ],
    [ "LYR", "classLYR.html", "classLYR" ],
    [ "Memcache", "classMemcache.html", "classMemcache" ],
    [ "MINMAX", "classMINMAX.html", "classMINMAX" ],
    [ "OBJ", "classOBJ.html", "classOBJ" ],
    [ "OpenJPEGImage", "classOpenJPEGImage.html", "classOpenJPEGImage" ],
    [ "PFL", "classPFL.html", "classPFL" ],
    [ "QLT", "classQLT.html", "classQLT" ],
    [ "RawTile", "classRawTile.html", "classRawTile" ],
    [ "RGN", "classRGN.html", "classRGN" ],
    [ "ROT", "classROT.html", "classROT" ],
    [ "SDS", "classSDS.html", "classSDS" ],
    [ "Session", "structSession.html", "structSession" ],
    [ "SHD", "classSHD.html", "classSHD" ],
    [ "SPECTRA", "classSPECTRA.html", "classSPECTRA" ],
    [ "Task", "classTask.html", "classTask" ],
    [ "TIL", "classTIL.html", "classTIL" ],
    [ "TileManager", "classTileManager.html", "classTileManager" ],
    [ "Timer", "classTimer.html", "classTimer" ],
    [ "Tokenizer", "classTokenizer.html", "classTokenizer" ],
    [ "TPTImage", "classTPTImage.html", "classTPTImage" ],
    [ "Transform", "structTransform.html", "structTransform" ],
    [ "URL", "classURL.html", "classURL" ],
    [ "View", "classView.html", "classView" ],
    [ "Watermark", "classWatermark.html", "classWatermark" ],
    [ "WID", "classWID.html", "classWID" ],
    [ "Writer", "classWriter.html", "classWriter" ],
    [ "Zoomify", "classZoomify.html", "classZoomify" ]
];