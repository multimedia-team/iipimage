var classOBJ =
[
    [ "bits_per_channel", "classOBJ.html#a1828fd62ca0ec2c71e5b1ef543dd81e0", null ],
    [ "colorspace", "classOBJ.html#a18187490f2d4f3f223dede668fe952d5", null ],
    [ "horizontal_views", "classOBJ.html#a0d9148a5e5c732cdec2eede2a078d1fc", null ],
    [ "iip", "classOBJ.html#ace6b1ccf028f4687d61cde5558e0c4dc", null ],
    [ "iip_server", "classOBJ.html#ab3107731a2c0745c784ce90ecb8ee5b8", null ],
    [ "max_size", "classOBJ.html#ad92188b93ad97c5838f4d2eda033915e", null ],
    [ "metadata", "classOBJ.html#acf95af49c5e127b613c606907bdc0c20", null ],
    [ "min_max_values", "classOBJ.html#aee0cff4657f4f086c45a84d27e5525c9", null ],
    [ "resolution_number", "classOBJ.html#abef7eebfdabad8b1daf4b44188f29124", null ],
    [ "resolutions", "classOBJ.html#ad62ba8e0fe74cb9d74c63e4d95b437b7", null ],
    [ "run", "classOBJ.html#ab6774ce96508d370ee5a68a7444952f7", null ],
    [ "tile_size", "classOBJ.html#a05ab00746c05e6c5c5e78c16a91dbe0f", null ],
    [ "vertical_views", "classOBJ.html#ab1ce958dfc16057355c91bf946e846ab", null ]
];