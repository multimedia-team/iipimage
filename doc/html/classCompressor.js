var classCompressor =
[
    [ "~Compressor", "classCompressor.html#a0610fdbd71526609ed20f0a3cd919360", null ],
    [ "addXMPMetadata", "classCompressor.html#af62307978b22d731e345eff148cab268", null ],
    [ "Compress", "classCompressor.html#af57b26b9e84ee92afa663324f464c3e0", null ],
    [ "CompressStrip", "classCompressor.html#a24549398c73442cf45b81e5cdeaf27c0", null ],
    [ "Finish", "classCompressor.html#a7a98fff52328aeea262452ee8949691d", null ],
    [ "getHeader", "classCompressor.html#a94e14df8f3dfe7968a9b77dabb4e2c45", null ],
    [ "getHeaderSize", "classCompressor.html#aeb0d63187b16a4de4ec27ea59295b1e5", null ],
    [ "getMimeType", "classCompressor.html#ade8725e65b19b79f3e481d519664b683", null ],
    [ "getQuality", "classCompressor.html#a70078e753e103bd4a642118d7412dd79", null ],
    [ "getSuffix", "classCompressor.html#aac704b00452d217cfbc5fb02a52505e1", null ],
    [ "InitCompression", "classCompressor.html#ad83402c75cd8f85c9263c1e990ae1e5f", null ],
    [ "setICCProfile", "classCompressor.html#af23c55b06260b6dd14b288bedf409b3b", null ],
    [ "setXMPMetadata", "classCompressor.html#afc78f825498b3a33380ef251d6a60f87", null ],
    [ "writeICCProfile", "classCompressor.html#a59d252c2c1499e02ca9f0ff0be144b23", null ],
    [ "writeXMPMetadata", "classCompressor.html#a61e5fb15b9b5838a79fe22a5a2430a58", null ],
    [ "icc", "classCompressor.html#a788d5559f4276c1498cf9bcdca2d42cb", null ],
    [ "Q", "classCompressor.html#a7a48db7e754ff8335485941f704a40a3", null ],
    [ "xmp", "classCompressor.html#a45698fa97911f4ecfe030c077c139c2b", null ]
];