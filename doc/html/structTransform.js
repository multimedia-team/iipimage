var structTransform =
[
    [ "binary", "structTransform.html#a3ac1decab1c97287baff8b1078a0af8d", null ],
    [ "cmap", "structTransform.html#a457a557dfab93e25e4745f64937862f5", null ],
    [ "contrast", "structTransform.html#aef17ac45981f521367b5b84292c09df8", null ],
    [ "equalize", "structTransform.html#afec763854d9c1014b228db409089a394", null ],
    [ "flatten", "structTransform.html#aaf78ed0c7e79553ccf6edc34b3223db0", null ],
    [ "flip", "structTransform.html#ac63c2058b3bfe1a896730e69bde3a8c5", null ],
    [ "gamma", "structTransform.html#a3dff2adee439080e103922910f01e107", null ],
    [ "getDescription", "structTransform.html#a79935b0501625a704e52d91f08c53f96", null ],
    [ "greyscale", "structTransform.html#a9b23b4901c60d29e17069a60aa796c4c", null ],
    [ "histogram", "structTransform.html#aa6c829f0bb60037219b2fbdc1e6523a2", null ],
    [ "interpolate_bilinear", "structTransform.html#ae7e5551efe57aef7a3f3f1ee6d8b766f", null ],
    [ "interpolate_nearestneighbour", "structTransform.html#ae57c6a03ac48bc1956ff8d384e6afead", null ],
    [ "inv", "structTransform.html#a0b70af250f14cf171f980b2a32c63d09", null ],
    [ "LAB2sRGB", "structTransform.html#a1b709fc6b2e17c8517091750e41d1c21", null ],
    [ "normalize", "structTransform.html#a91c6da0b9e8ec35414e79ef6e7af0433", null ],
    [ "rotate", "structTransform.html#a526efb8dbfcca9d29890a791e212c30e", null ],
    [ "shade", "structTransform.html#aec3ef98eb69767a1b3f563cf4ee9694e", null ],
    [ "threshold", "structTransform.html#a35876de0838e52cf8489ca9d71dfb7f5", null ],
    [ "twist", "structTransform.html#a406c76986276c32665d6a5b16d978c32", null ]
];